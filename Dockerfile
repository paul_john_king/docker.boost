# © 2019 Paul John King (paul_john_king@web.de).  All rights reserved.
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License, version 3 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

# TODO: Find some way to verify the download.  There is an `.asc` file, but I
# cannot find the public key to unlock it.  There is an SHA256 checksum, but it
# is part of a web page, not in a file, and so will require a scraping script.

ARG PROJECT_NAME
ARG PROJECT_VERSION
ARG PROJECT_URL

ARG WORKBENCH_IMAGE="registry.gitlab.com/paul_john_king/docker.ubuntu_workbench:0.0.18_7efec42"

ARG BOOST_MAJOR="1"
ARG BOOST_MINOR="70"
ARG BOOST_PATCH="0"
ARG BOOST_VARIANT="release"
ARG BOOST_THREADING="multi"

ARG WORK_DIR="/work"
ARG DOWNLOAD_DIR="${WORK_DIR}/downloads"
ARG OBJECT_DIR="${WORK_DIR}/objects"
ARG SOURCE_DIR="${WORK_DIR}/sources"
ARG TARGET_DIR="${WORK_DIR}/targets"

FROM "${WORKBENCH_IMAGE}" AS compile

	ARG BOOST_MAJOR
	ARG BOOST_MINOR
	ARG BOOST_PATCH
	ARG BOOST_VARIANT
	ARG BOOST_THREADING

	ARG WORK_DIR
	ARG DOWNLOAD_DIR
	ARG SOURCE_DIR
	ARG OBJECT_DIR
	ARG TARGET_DIR

	ARG BOOST_NAME="boost_${BOOST_MAJOR}_${BOOST_MINOR}_${BOOST_PATCH}"
	ARG BOOST_DOWNLOADS="https://dl.bintray.com/boostorg/release/${BOOST_MAJOR}.${BOOST_MINOR}.${BOOST_PATCH}/source"

	ARG BOOST_ARCHIVE="${BOOST_NAME}.tar.bz2"
	ARG BOOST_ARCHIVE_URL="${BOOST_DOWNLOADS}/${BOOST_ARCHIVE}"
	ARG BOOST_ARCHIVE_PATH="${DOWNLOAD_DIR}/${BOOST_ARCHIVE}"

	RUN \
		set -e; \
		set -u; \
		mkdir -p "${DOWNLOAD_DIR}" "${SOURCE_DIR}" "${OBJECT_DIR}" "${TARGET_DIR}"; \
		cd "${DOWNLOAD_DIR}"; \
		curl --remote-name --location "${BOOST_ARCHIVE_URL}"; \
		cd "${SOURCE_DIR}"; \
		tar -xf "${BOOST_ARCHIVE_PATH}"; \
		cd "${BOOST_NAME}"; \
		./bootstrap.sh --prefix="${TARGET_DIR}" ;\
		./b2 --build-dir="${OBJECT_DIR}" \
			variant="${BOOST_VARIANT}" \
			threading="${BOOST_THREADING}" \
			install ;\
		return;

FROM "scratch"

	ARG PROJECT_NAME
	ARG PROJECT_VERSION
	ARG PROJECT_URL

	ARG WORKBENCH_IMAGE

	ARG BOOST_MAJOR
	ARG BOOST_MINOR
	ARG BOOST_PATCH
	ARG BOOST_VARIANT
	ARG BOOST_THREADING

	ARG TARGET_DIR

	LABEL \
		project.name="${PROJECT_NAME}" \
		project.version="${PROJECT_VERSION}" \
		project.url="${PROJECT_URL}" \
		workbench.image="${WORKBENCH_IMAGE}" \
		boost.version="${BOOST_MAJOR}.${BOOST_MINOR}.${BOOST_PATCH}" \
		boost.variant="${BOOST_VARIANT}" \
		boost.threading="${BOOST_THREADING}"

	COPY --from="compile" "${TARGET_DIR}" "/"
